package com.xperi.collection.controller;

import com.xperi.collection.constants.CollectionStatusConstants;
import com.xperi.collection.dto.CollectionDto;
import com.xperi.collection.entity.CollectionEntity;
import com.xperi.collection.service.CollectionManagerService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * This is a test class for Collection Manager Controller
 *
 */

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing Controller Class : CollectionManagerController")
public class CollectionManagerControllerTest {

    @InjectMocks
    private CollectionManagerController collectionManagerController;

    @Mock
    private CollectionManagerService collectionManagerService;


    /**
     * Test method for create collection API
     *
     */
    @SneakyThrows
    @Test
    @DisplayName("Testing createCollection method")
    public void testCreateCollection()
    {
        //Test object
        CollectionEntity testCollection = new CollectionEntity();

        testCollection.setName("testName");
        testCollection.setDescription("testDescription");
        testCollection.setOwnerId("testOwnerId");
        testCollection.setContentType("testContentType");
        testCollection.setCreatedBy("testUser");

        when(collectionManagerService.createCollection(any()))
                .thenReturn(testCollection);

        CollectionDto dummyCollectionDto = new CollectionDto();
        ResponseEntity<CollectionEntity> result = collectionManagerController.createCollection(dummyCollectionDto);


        assertEquals(testCollection.getName(),result.getBody().getName());
        assertEquals(testCollection.getDescription(),result.getBody().getDescription());
        assertEquals(testCollection.getOwnerId(), result.getBody().getOwnerId());
        assertEquals(testCollection.getContentType(),result.getBody().getContentType());
        assertEquals(testCollection.getCreatedBy(),result.getBody().getCreatedBy());
        assertEquals(CollectionStatusConstants.ACTIVE.getValue(),result.getBody().getStatus());
        assertEquals(Boolean.FALSE,result.getBody().isDeleted());
        assertEquals(Boolean.FALSE, result.getBody().isEncrypted());
        assertEquals(testCollection.getVersionId(),result.getBody().getVersionId());
        assertEquals(HttpStatus.CREATED,result.getStatusCode());

    }
}
