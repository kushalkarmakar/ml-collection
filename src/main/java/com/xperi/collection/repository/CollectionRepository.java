package com.xperi.collection.repository;

import com.xperi.collection.entity.CollectionEntity;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository for collection related operation
 */
@Repository
public interface CollectionRepository extends CassandraRepository<CollectionEntity, UUID> {
}


