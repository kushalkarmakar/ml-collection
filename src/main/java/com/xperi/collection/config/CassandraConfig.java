package com.xperi.collection.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.SchemaAction;

/**
 * This CassandraConfig class specifies the Spring Data Cassandra configuration
 */

@Configuration
public class CassandraConfig extends AbstractCassandraConfiguration {
    /**
     * To specify Cassandra Contact points
     */
    //@Value("${spring.data.cassandra.contact-points:placeholder}")
    private String contactPoints;

    /**
     * To specify the port on which Cassandra is running
     */
    //@Value("${spring.data.cassandra.port:0000}")
    private int port;

    /**
     * To specify Cassandra keyspace
     */
    //@Value("${spring.data.cassandra.keyspace:placeholder}")
    private String keySpace;

    /**
     * To specify Cassandra username
     */
    //@Value("${spring.data.cassandra.username}")
    private String username;

    /**
     * To specify Cassandra password
     */
    //@Value("${spring.data.cassandra.password}")
    private String password;

    /**
     * To specify Cassandra schema action
     */
    //@Value("${spring.data.cassandra.schema-action}")
    private String schemaAction;


    /**
     * setting the properties value of Cassandra configuration from Environment variables
     */
    CassandraConfig() {
        this.contactPoints = System.getenv("CASSANDRA_CONTACT_POINTS");
        this.port = Integer.parseInt(System.getenv("CASSANDRA_PORT"));
        this.keySpace = System.getenv("CASSANDRA_KEYSPACE");
        this.username = System.getenv("CASSANDRA_USERNAME");
        this.password = System.getenv("CASSANDRA_PASSWORD");
        this.schemaAction = System.getenv("CASSANDRA_SCHEMA_ACTION");
    }


    /**
     * Returns the Cassandra Keyspace name
     *
     * @return String
     */
    @Override
    protected String getKeyspaceName() {
        return keySpace;
    }

    /**
     * Returns the Cassandra Contact points
     *
     * @return String
     */
    @Override
    protected String getContactPoints() {
        return contactPoints;
    }

    /**
     * Returns the port number on which Cassandra is running
     *
     * @return int
     */
    @Override
    protected int getPort() {
        return port;
    }

    /**
     * Returns the Cassandra schema action
     *
     * @return SchemaAction
     */
    @Override
    public SchemaAction getSchemaAction() {
        return SchemaAction.valueOf(schemaAction);
    }

}
