package com.xperi.collection.util;

import com.xperi.collection.dto.CollectionDto;
import com.xperi.collection.entity.CollectionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


/**
 * Mapper class to map CollectionDto object with the Xperi Collection model
 */
@Mapper(componentModel = "spring")
public interface CollectionDtoMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "status", ignore = true),
            @Mapping(target = "versionId", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "encrypted", ignore = true),
            @Mapping(target = "lastModified", ignore = true),
            @Mapping(target = "deleted", ignore = true),
            @Mapping(target = "name", source = "dto.name"),
            @Mapping(target = "description", source = "dto.description"),
            @Mapping(target = "ownerId", source = "dto.ownerId"),
            @Mapping(target = "contentType", source = "dto.contentType"),
            @Mapping(target = "createdBy", source = "dto.createdBy")
    })
    CollectionEntity collectionDtoToCollection(CollectionDto dto);
}
