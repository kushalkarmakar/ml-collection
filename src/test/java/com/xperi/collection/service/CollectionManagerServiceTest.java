package com.xperi.collection.service;

import com.xperi.collection.constants.CollectionStatusConstants;
import com.xperi.collection.dto.CollectionDto;
import com.xperi.collection.exceptions.CreateCollectionException;
import com.xperi.collection.entity.CollectionEntity;
import com.xperi.collection.repository.CollectionRepository;
import com.xperi.collection.util.CollectionDtoMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoInteractions;


/**
 * This is a test class for Collection Manager Service
 *
 */

@ExtendWith(MockitoExtension.class)
@DisplayName("Testing Service Class : CollectionManagerService")
public class CollectionManagerServiceTest {

    @InjectMocks
    private CollectionManagerService collectionManagerService;

    @Mock
    private CollectionRepository repository;

    @Mock
    private CollectionDtoMapper collectionDtoMapper;


    /**
     * Test method for creating a collection without throwing any exception
     *
     */
    @SneakyThrows
    @Test
    @DisplayName("Testing createCollection without throwing exception")
    public void testCreateCollectionWithoutThrowingException(){

        //Test object
        CollectionEntity testCollection = new CollectionEntity();

        testCollection.setName("testName");
        testCollection.setDescription("testDescription");
        testCollection.setOwnerId("testOwnerId");
        testCollection.setContentType("testContentType");
        testCollection.setCreatedBy("testUser");

        CollectionDto dummyCollectionDto = new CollectionDto();

        when(collectionDtoMapper.collectionDtoToCollection(any()))
                .thenReturn(testCollection);

        CollectionEntity result = collectionManagerService.createCollection(dummyCollectionDto);

        assertEquals(testCollection.getName(),result.getName());
        assertEquals(testCollection.getDescription(),result.getDescription());
        assertEquals(testCollection.getOwnerId(), result.getOwnerId());
        assertEquals(testCollection.getContentType(),result.getContentType());
        assertEquals(testCollection.getCreatedBy(),result.getCreatedBy());
        assertEquals(CollectionStatusConstants.ACTIVE.getValue(),result.getStatus());
        assertEquals(Boolean.FALSE,result.isDeleted());
        assertEquals(Boolean.FALSE, result.isEncrypted());
        assertEquals(testCollection.getVersionId(),result.getVersionId());
        verify(repository, times(1)).insert(any(CollectionEntity.class));
    }

    /**
     * Test method for creating a collection with throwing an exception
     *
     */
    @SneakyThrows
    @Test
    @DisplayName("Testing createCollection with throwing exception")
    public void testCreateCollectionWithThrowingException(){

        //Test object
        CollectionEntity testCollection = new CollectionEntity();
        testCollection.setName("testName");
        testCollection.setDescription("testDescription");
        testCollection.setOwnerId("testOwnerId");
        testCollection.setContentType("testContentType");
        testCollection.setCreatedBy("testUser");

        CollectionDto dummyCollectionDto = new CollectionDto();

        when(collectionDtoMapper.collectionDtoToCollection(any()))
                .thenThrow(NullPointerException.class);
        verifyNoInteractions(repository);
        assertThrows(CreateCollectionException.class,() -> {
            collectionManagerService.createCollection(dummyCollectionDto);
        });
    }
}
