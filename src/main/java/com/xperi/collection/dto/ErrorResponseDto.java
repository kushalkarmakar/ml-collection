package com.xperi.collection.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


/**
 * This is a custom error response class
 */

@Data
@AllArgsConstructor
public class ErrorResponseDto {
    private List<String> message;
    private String details;
}