package com.xperi.collection.controller;

import com.xperi.collection.dto.CollectionDto;
import com.xperi.collection.exceptions.CreateCollectionException;
import com.xperi.collection.entity.CollectionEntity;
import com.xperi.collection.dto.ErrorResponseDto;
import com.xperi.collection.service.CollectionManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This controller contains all the collection related APIs
 */

@RestController
@RequestMapping("/api/collection-manager")
@Slf4j
public class CollectionManagerController {

    @Autowired
    private CollectionManagerService collectionManagerService;

    /**
     * This API will create a Collection entity in the Cassandra
     *
     * @param collectionDto to takes necessary request parameters to create a collection
     * @return It returns the same Collection entity after creating a collection in database
     * @throws CreateCollectionException
     */
    @PostMapping("/create-collection")
    public ResponseEntity<CollectionEntity> createCollection(@Valid @RequestBody CollectionDto collectionDto) throws Exception {
        log.debug("--- request details for creating collection  :  {}", collectionDto);
        return new ResponseEntity<>(collectionManagerService.createCollection(collectionDto), HttpStatus.CREATED);
    }

    /**
     * It checks the Argument validation for create collection API
     *
     * @param ex MethodArgumentNotValidException for validation error
     * @return error response with appropriate status code
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponseDto> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult().getAllErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.toList());
        ErrorResponseDto error = new ErrorResponseDto(errors, ex.getLocalizedMessage());
        log.error("--- error occurred when creating a collection : {}", ex, ex.getMessage());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
