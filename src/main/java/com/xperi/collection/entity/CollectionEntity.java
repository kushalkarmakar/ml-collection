package com.xperi.collection.entity;


import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.xperi.collection.constants.CollectionStatusConstants;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.springframework.data.cassandra.core.mapping.*;

import javax.persistence.Entity;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


/**
 * This is XPERI Collection model class
 */

@Entity
@Data
@Table("collection")
@AllArgsConstructor
public class CollectionEntity {
    /*
     * It must have a authentication authorization property, collection id
     * should be uniquely identified in the system */
    @PrimaryKey
    @Column(value = "id")
    private UUID id;

    /* Name of the collection */
    @Column(value = "name")
    private String name;

    /* Collection status */
    @Column(value = "status")
    private String status;

    /* Collection unique Owner Identifier */
    @Column(value = "ownerId")
    private String ownerId;

    /* Collection unique version identifier */
    @Column(value = "versionId")
    private String versionId;

    /* Collection create at Datetime */
    @Column(value = "createdAt")
    private Date createdAt;

    /* Boolean field to indicate if the Collection is encrypted */
    @Column(value = "encrypted")
    private boolean encrypted;

    /* Content type of the collection.  example image, video, audio, document etc */
    @Column(value = "contentType")
    private String contentType;

    /* when the collection was last modified Date/time */
    @Column(value = "lastModified")
    private Date lastModified;

    /* Flag to indicate if the collection is marked for deletion */
    @Column(value = "deleted")
    private boolean deleted;

    /* Describe the Collection */
    @Column(value = "description")
    private String description;

    /* the user Id of the who created the collection. */
    @Column(value = "createdBy")
    private String createdBy;


    /**
     * To set default values in the new Collection
     */
    public CollectionEntity() {
        this.id = Uuids.timeBased();
        this.status = CollectionStatusConstants.ACTIVE.getValue();
        this.createdAt = Calendar.getInstance().getTime();
        this.deleted = false;
        this.encrypted = false;
        this.lastModified = Calendar.getInstance().getTime();
        this.versionId = "1.0.0";
    }
}
