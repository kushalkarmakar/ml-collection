package com.xperi.collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MlCollectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MlCollectionApplication.class, args);
	}

}
