package com.xperi.collection.constants;

/**
 * This CollectionStatusConstants enum specifies the Collection Status Values
 */

public enum CollectionStatusConstants {
    // For Active and Inactive Collection values
    ACTIVE("Active"),
    INACTIVE("Inactive");

    public final String value;

    private CollectionStatusConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
