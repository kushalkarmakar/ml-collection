package com.xperi.collection.service;

import com.xperi.collection.dto.CollectionDto;
import com.xperi.collection.exceptions.CreateCollectionException;
import com.xperi.collection.entity.CollectionEntity;
import com.xperi.collection.repository.CollectionRepository;
import com.xperi.collection.util.CollectionDtoMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * This class contains all the collection related services
 */

@Slf4j
@Service
public class CollectionManagerService {

    @Autowired
    private CollectionRepository repository;

    @Autowired
    private CollectionDtoMapper collectionDtoMapper;


    /**
     * This method is to create a collection in the cassandra
     *
     * @param collectionDto object received through request body
     * @return Collection object created in database
     * @throws CreateCollectionException
     */
    public CollectionEntity createCollection(CollectionDto collectionDto) throws CreateCollectionException {
        try {
            CollectionEntity collection = collectionDtoMapper.collectionDtoToCollection(collectionDto);
            log.debug("--- collection details before creating entry in database : {}", collection);
            repository.insert(collection);
            return collection;
        } catch (Exception ex) {
            log.error("--- error occurred when creating a collection : {}", ex,ex.getMessage());
            throw new CreateCollectionException(ex);
        }
    }

}
