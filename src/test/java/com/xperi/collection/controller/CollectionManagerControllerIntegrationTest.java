package com.xperi.collection.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.xperi.collection.dto.CollectionDto;
import com.xperi.collection.entity.CollectionEntity;
import com.xperi.collection.service.CollectionManagerService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(CollectionManagerController.class)
public class CollectionManagerControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CollectionManagerService service;

    /**
     * Test method for create collection API
     */
    @SneakyThrows
    @Test
    @DisplayName("Testing create collection API")
    public void testCreateCollection() {
        // Test object - dto
        CollectionDto collectionDto = new CollectionDto();
        collectionDto.setName("testName");
        collectionDto.setDescription("testDescription");
        collectionDto.setOwnerId("testOwnerId");
        collectionDto.setContentType("testContentType");
        collectionDto.setCreatedBy("testUser");

        // Collection object dto
        CollectionEntity collection = new CollectionEntity();
        collection.setName("testName");
        collection.setDescription("testDescription");
        collection.setOwnerId("testOwnerId");
        collection.setContentType("testContentType");
        collection.setCreatedBy("testUser");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(collectionDto);

        given(service.createCollection(Mockito.any())).willReturn(collection);

        mvc.perform(post("/api/collection-manager/create-collection")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(collection.getName()))
                .andExpect(jsonPath("$.description").value(collection.getDescription()))
                .andExpect(jsonPath("$.contentType").value(collection.getContentType()));

    }
}
