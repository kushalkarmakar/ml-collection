package com.xperi.collection.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * This is a generic error response class
 */
@Data
@AllArgsConstructor
public class ErrorDetailsDto {
    private Date timestamp;
    private String error;
    private String message;
}
