package com.xperi.collection.exceptions;

/**
 * Custom Exception class for creating collection
 */
public class CreateCollectionException extends Exception {
    public CreateCollectionException(Exception ex) {
        super(ex);
    }
}