FROM openjdk:15.0.1-slim

RUN mkdir /app
WORKDIR /app

RUN apt-get update && apt-get -y install wget && apt-get -y install unzip && apt-get clean

RUN wget https://services.gradle.org/distributions/gradle-6.8.2-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip

ENV GRADLE_HOME=/opt/gradle/gradle-6.8.2
ENV PATH=${GRADLE_HOME}/bin:${PATH}

RUN gradle -v

COPY src /app/src
COPY build.gradle settings.gradle gradlew /app/

RUN gradle clean
RUN gradle build -x test
RUN gradle clean jar

EXPOSE 9700
ENTRYPOINT ["java" , "-jar" , "build/libs/ML-collection-0.0.1-SNAPSHOT.jar"]