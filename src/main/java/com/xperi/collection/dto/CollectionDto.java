package com.xperi.collection.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * This is a DTO class for creating Collection
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectionDto {
    @NotNull(message = "collection name parameter is required")
    @NotBlank(message = "collection name can not be blank")
    private String name;

    @NotNull(message = "collection description parameter is required")
    @NotBlank(message = "collection description can not be blank")
    private String description;

    @NotNull(message = "collection ownerId parameter is required")
    @NotBlank(message = "collection ownerId can not be blank")
    private String ownerId;

    @NotNull(message = "collection contentType parameter is required")
    @NotBlank(message = "collection contentType can not be blank")
    private String contentType;

    @NotNull(message = "collection createdBy parameter is required")
    @NotBlank(message = "collection createdBy can not be blank")
    private String createdBy;
}
